<?php

namespace Tests\Feature;

use App\Models\Student;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class FixOrderTest extends TestCase
{
    /**
     * Index.
     *
     * @return void
     */
    public function test_order()
    {
        //Add 10 rows
        Student::factory(10)->create();
        //Get this rows
        $students = Student::orderBy('id', 'desc')->limit(10)->get();
        // Get middle row from last 10 students
        $get_row_in_middle = Student::orderBy('id', 'desc')->skip(4)->take(1)->first();
        $school_id = $get_row_in_middle->school_id;
        //Delete this middle row
        $get_row_in_middle->delete();
        //Run command which fix order
        Artisan::queue('rearrange:student', [
            'school' => [$school_id]
        ]);
        //Get all students in this school
        $students = Student::where('school_id', $school_id)->get();
        //Confirm arrange order
        foreach ($students as $key => $student) {
            $this->assertEquals($key+1, $student->order);
        }
    }


}
