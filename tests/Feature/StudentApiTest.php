<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\School;
use App\Models\Student;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class StudentApiTest extends TestCase
{
    /**
     * Authentication
     *
     * @return void
     */
    public function test_get_auth_error()
    {
        $response = $this->getJson('/api/students');
        $response->assertStatus(401);
    }

    /**
     * Index.
     *
     * @return void
     */
    public function test_index()
    {
        // $response = $this->getJson('/api/students', ['Authorization' =>
        // 'Bearer YAHvgBGNlgFbxDnIMJq4XYSDHNyXP7GlZxRtoqlX']);
        Sanctum::actingAs(User::factory()->create());
        $response = $this->getJson('/api/students');
        $response
           ->assertStatus(200)
           ->assertJson([
               'status' => true,
           ]);
    }

    /**
     * Create.
     *
     * @return void
     */
    public function test_create()
    {
        Sanctum::actingAs(User::factory()->create());
        $response = $this->getJson('/api/students/create');

        $response
           ->assertStatus(200)
           ->assertJson([
               'status' => true,
           ]);
    }

    /**
     * Store without send data
     *
     * @return void
     */
    public function test_store_without_content()
    {
        Sanctum::actingAs(User::factory()->create());
        $response = $this->postJson('/api/students');
        $response->assertStatus(400);
    }

    /**
     * Store with error data
     *
     * @return void
     */
    public function test_store_error_validation_content()
    {
        Sanctum::actingAs(User::factory()->create());
        $school = School::orderBy('id', 'desc')->first();
        $response = $this->postJson('/api/students', [
            'name'      => 'Wael Serag',
            'school_id' => $school->id+1
        ]);
        $response->assertStatus(400);
    }

    /**
     * Store
     *
     * @return void
     */
    public function test_store()
    {
        $old_count_students = Student::count();
        $school = School::first();
        Sanctum::actingAs(User::factory()->create());
        $response = $this->postJson('/api/students', [
            'name'      => 'Wael Serag',
            'school_id' => $school->id
        ]);
        $new_count_students = Student::count();
        $this->assertEquals($old_count_students+1, $new_count_students);
        $response
           ->assertStatus(201)
           ->assertJson([
               'status' => true,
           ]);
    }

    /**
     * Edit.
     *
     * @return void
     */
    public function edit()
    {
        $student = Student::first();
        $response = $this->getJson('/api/students/'.$student->id.'/edit');

        $response
           ->assertStatus(200)
           ->assertJson([
               'status' => true,
           ]);
    }

    /**
     * Update without send data
     *
     * @return void
     */
    public function test_update_without_content()
    {
        $student = Student::first();
        Sanctum::actingAs(User::factory()->create());
        $response = $this->patchJson('/api/students/'.$student->id);
        $response->assertStatus(400);
    }

    /**
     * Update with error data
     *
     * @return void
     */
    public function test_update_error_validation_content()
    {
        Sanctum::actingAs(User::factory()->create());
        $student = Student::first();
        $school = School::orderBy('id', 'desc')->first();
        $response = $this->patchJson('/api/students/'.$student->id, [
            'name'      => 'Wael Serag',
            'school_id' => $school->id+1
        ]);
        $response->assertStatus(400);
    }

    /**
     * Update
     *
     * @return void
     */
    public function test_update()
    {
        Sanctum::actingAs(User::factory()->create());
        $student = Student::first();
        $old_count_students = Student::count();
        $school = School::first();
        $response = $this->patchJson('/api/students/'.$student->id, [
            'name'      => 'Wael Serag',
            'school_id' => $school->id
        ]);
        $new_count_students = Student::count();
        $this->assertEquals($old_count_students, $new_count_students);
        $response
           ->assertStatus(200)
           ->assertJson([
               'status' => true,
           ]);
    }

    /**
     * Destroy
     *
     * @return void
     */
    public function test_destroy()
    {
        Sanctum::actingAs(User::factory()->create());
        $student = Student::first();
        $old_count_students = Student::count();
        $response = $this->deleteJson('/api/students/'.$student->id);
        $new_count_students = Student::count();
        $this->assertEquals($old_count_students, $new_count_students+1);
        $response
           ->assertStatus(200)
           ->assertJson([
               'status' => true,
           ]);
    }
}
