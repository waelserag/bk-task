@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center">Edit School</h1>
                <a href="{{ route('schools.index') }}"> Schools </a>
                <form class="form" action="{{ route('schools.update',[$school->id]) }}" method="post">
                    @csrf
                    @method('PATCH')
                    @include('schools/_form')
                    <div class="form-group mt-3">
                         <button type="submit" class="btn btn-primary mb-2">Edit</button>
                    </div>
               </form>
            </div>
        </div>
    </div>
@endsection
