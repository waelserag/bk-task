@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center">Schools</h1>
                <a href="{{ route('schools.create') }}"> Add School </a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($schools as $school)
                            <tr>
                                <th scope="row">{{ $school->id }}</th>
                                <td>{{ $school->name }}</td>
                                <td><a href="{{ route('schools.edit',[$school->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a></td>
                                <td>
                                    <form class="" action="{{ route('schools.destroy',[$school->id]) }}" method="post">
                                        @csrf
                                        @method('Delete')
                                        <button type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $schools->links() }}
            </div>
        </div>
    </div>
@endsection
