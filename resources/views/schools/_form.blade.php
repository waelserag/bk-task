
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" required value="{{ old('name', $school->name) }}"
            class="form-control mt-2" id="name" placeholder="School Name">
    </div>
