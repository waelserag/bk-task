@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center">Edit Student</h1>
                <a href="{{ route('students.index') }}"> Students </a>
                <form class="form" action="{{ route('students.update',[$student->id]) }}" method="post">
                    @csrf
                    @method('PATCH')
                    @include('students/_form')
                    <div class="form-group mt-3">
                         <button type="submit" class="btn btn-primary mb-2">Edit</button>
                    </div>
               </form>
            </div>
        </div>
    </div>
@endsection
