@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="text-center">Students</h1>
                <a href="{{ route('students.create') }}"> Add Student </a>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">id</th>
                            <th scope="col">Name</th>
                            <th scope="col">School</th>
                            <th scope="col">Order</th>
                            <th scope="col">Edit</th>
                            <th scope="col">Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($students as $student)
                            <tr>
                                <th scope="row">{{ $student->id }}</th>
                                <td>{{ $student->name }}</td>
                                <td>{{ $student->School->name }}</td>
                                <td>{{ $student->order }}</td>
                                <td><a href="{{ route('students.edit',[$student->id]) }}">
                                    <i class="fa fa-edit"></i>
                                </a></td>
                                <td>
                                    <form class="" action="{{ route('students.destroy',[$student->id]) }}" method="post">
                                        @csrf
                                        @method('Delete')
                                        <button type="submit">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $students->links() }}
            </div>
        </div>
    </div>
@endsection
