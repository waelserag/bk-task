
    <div class="form-group mb-3">
        <label for="name">Name</label>
        <input type="text" name="name" required value="{{ old('name', $student->name) }}"
            class="form-control mt-2" id="name" placeholder="Student Name">
    </div>
    <div class="form-group">
        <label for="school_id">School</label>
        <select class="form-control" name="school_id">
            @foreach ($schools as $school)
                <option value="{{ $school->id }}"  {{ $school->id == old('school_id', $student?->school_id) ? 'selected' : '' }}>
                    {{ $school->name }}
                </option>
            @endforeach
        </select>
    </div>
