<?php

namespace App\Console\Commands;

use App\Models\School;
use App\Models\Student;
use Illuminate\Console\Command;

class RearrangeStudents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rearrange:student {school*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rearrange students for schools';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $schools = $this->argument('school');
        foreach ($schools as $key => $school) {
            $students = Student::where('school_id', $school)->get();
            foreach ($students as $key => $student) {
                Student::whereId($student->id)->update(['order' => $key+1]);
            }
        }
    }
}
