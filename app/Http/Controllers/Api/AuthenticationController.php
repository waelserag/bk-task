<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class AuthenticationController extends Controller
{
    use ApiResponse;

    /**
    * Sign In APi
    * url: api/signin
    * @method post
    * content: email, password
    * @return Illuminate\Http\JsonResponse
    */
    public function signin(Request $request) :JsonResponse
    {
        $rules = array(
            'email' => 'required|string|email|',
            'password' => 'required|string|min:6'
        );
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
        {
            return $this->error("Validation Error", 400, $validator->messages()->all());
        }

        if (!auth()->attempt($request->all())) {
            return $this->error('Credentials not match', 401);
        }

        return $this->success(
            'Success',
            [
            'token' => auth()->user()->createToken('API Token')->plainTextToken
            ]
        );
    }
}
