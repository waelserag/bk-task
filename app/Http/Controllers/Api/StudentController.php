<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StudentRequest;
use App\Http\Resources\StudentResource;
use App\Http\Services\StudentService;
use App\Http\Traits\ApiResponse;
use App\Http\Traits\FixOrder;
use App\Models\School;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;

class StudentController extends Controller
{
    use ApiResponse, FixOrder;

    public function __construct(public StudentService $StudentService){}

    /**
    * Display a listing of the resource.
    * url: api/students
    * @method get
    * @return Illuminate\Http\JsonResponse
    */
    public function index() :JsonResponse
    {
        $students = new StudentResource(Student::with('school')->paginate(10));
        return $this->success('Get Data', $students);
    }

    /**
     * Show the form for creating a new resource.
     * url: api/students/create
     * @method get
     * @return Illuminate\Http\JsonResponse
     */
    public function create() :JsonResponse
    {
        $schools = School::get(['id','name']);
        return $this->success('Get Data', ["schools" => $schools]);
    }

    /**
     * Store a newly created resource in storage.
     * url: api/students
     * @method post
     * content: name,school_id
     * @param  \App\Http\Requests\Api\StudentRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StudentRequest $request) :JsonResponse
    {
        $response = $this->StudentService->fillFromRequest($request);
        if ($response) {
            return $this->success('Success', $response, 201);
        }
        return $this->error('Please Check Your Data', 400);
    }

    /**
     * Show the form for editing the specified resource.
     * url: api/students/{id}/edit
     * @method get
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Student $student) :JsonResponse
    {
        $schools = School::get(['id','name']);
        return $this->success('Get Data',
            [
                'schools' => $schools,
                'student' => $student,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     * url: api/students/{id}
     * @method patch
     * content: name,school_id
     * @param  \App\Http\Requests\Api\StudentRequest  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(StudentRequest $request, Student $student) :JsonResponse
    {
        $response = $this->StudentService->fillFromRequest($request, $student);
        //Fix order through command then send email to admin
        $this->fix([$student->school_id, $request->school_id]);
        if ($response) {
            return $this->success('Success', $response, 200);
        }
        return $this->error('Please Check Your Data', 400);
    }

    /**
     * Remove the specified resource from storage.
     * url: api/students/{id}
     * @method delete
     * @param  \App\Models\Student  $student
     * @return @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Student $student) :JsonResponse
    {
        $delete = $student->delete();
        if ($delete) {
            //Fix order through command then send email to admin
            $this->fix([$student->school_id]);
            return $this->success('Success', $delete, 200);
        }
        return $this->error('Please Check Your Data', 400);
    }
}
