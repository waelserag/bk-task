<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Http\Resources\StudentCollectionResource;
use App\Http\Services\StudentService;
use App\Http\Traits\FixOrder;
use App\Models\Student;
use App\Models\School;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StudentController extends Controller
{
    use FixOrder;

    public function __construct(public StudentService $StudentService){}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() :View
    {
        $students = new StudentCollectionResource(Student::with('school')->paginate(10));
        return view('students/index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() :view
    {
        $student = new Student();
        $schools = School::get();
        return view('students/create', compact('student', 'schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StudentRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StudentRequest $request) :RedirectResponse
    {
        $response = $this->StudentService->fillFromRequest($request);
        if ($response) {
            return redirect()->route('students.index')->with([
               'success' => "Created Successfully"
               ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\View\View
     */
    public function edit(Student $student) :view
    {
        $schools = School::get();
        return view('students/edit', compact('student', 'schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StudentRequest  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(StudentRequest $request, Student $student) :RedirectResponse
    {
        $response = $this->StudentService->fillFromRequest($request, $student);
        //Fix order through command then send email to admin
        $this->fix([$student->school_id, $request->school_id]);
        if ($response) {
            return redirect()->route('students.index')->with([
               'success' => "Updated Successfully"
               ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Student $student) :RedirectResponse
    {
        $student->delete();
        //Fix order through command then send email to admin
        $this->fix([$student->school_id]);
        return redirect()->route('students.index')->with([
           'success' => "Deleted Successfully"
        ]);
    }
}
