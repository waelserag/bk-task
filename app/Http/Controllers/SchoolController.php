<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\Http\Resources\SchoolCollectionResource;
use App\Http\Services\SchoolService;
use App\Models\School;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class SchoolController extends Controller
{
    public function __construct(public SchoolService $SchoolService){}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() :View
    {
        $schools = new SchoolCollectionResource(School::paginate(10));
        return view('schools/index',compact('schools'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() :view
    {
        $school = new School();
        return view('schools/create', compact('school'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SchoolRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SchoolRequest $request) :RedirectResponse
    {
        $response = $this->SchoolService->fillFromRequest($request);
        if ($response) {
            return redirect()->route('schools.index')->with([
               'success' => "Created Successfully"
               ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\View\View
     */
    public function edit(School $school) :view
    {
        return view('schools/edit', compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SchoolRequest  $request
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SchoolRequest $request, School $school) :RedirectResponse
    {
        $response = $this->SchoolService->fillFromRequest($request, $school);
        if ($response) {
            return redirect()->route('schools.index')->with([
               'success' => "Updated Successfully"
               ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School  $school
     * @return @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(School $school) :RedirectResponse
    {
        $school->delete();
        return redirect()->route('schools.index')->with([
           'success' => "Deleted Successfully"
           ]);
    }
}
