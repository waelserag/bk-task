<?php

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;

trait ApiResponse
{
    /**
    * send success response
    * @param  string $message
    * @param  array or Resource $data
    * @param  int $staus
    * @return Illuminate\Http\JsonResponse
    */
    public function success(string $message, $data, int $status = 200) :JsonResponse
    {
        return response()->json([
            "status" => true,
            "message" => $message,
            "data"    => $data,
        ],$status);
    }

    /**
    * send error response
    * @param  string $message
    * @param  array $errors
    * @param  int $staus
    * @return Illuminate\Http\JsonResponse
    */
    public function error(string $message ,int $status = 400, array $errors = null) :JsonResponse
    {
        return response()->json([
            "status" => false,
            "message" => $message,
            "errors" => $errors ? $errors :[$message]
        ],$status);
    }
}
