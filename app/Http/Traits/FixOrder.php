<?php

namespace App\Http\Traits;

use App\Events\SendEmailEvent;
use Illuminate\Support\Facades\Artisan;

trait FixOrder
{
    /**
    * Fix Order With Command
    * @param  array $school_ids
    * @return void
    */
    public function fix(array $school_ids) :void
    {
        Artisan::queue('rearrange:student', [
            'school' => $school_ids
        ]);
        $this->sendEmail();
    }

    /**
    * Send Email
    * @return void
    */
    public function sendEmail() :void
    {
        event(new SendEmailEvent());
    }
}
