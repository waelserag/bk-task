<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'name'      => 'required|string|min:3|max:255',
            'school_id' => 'required|exists:schools,id',
        ];
    }

    /**
     * Rename Attribute
     */
    public function attributes()
    {
        return [
            'school_id' => 'school',
        ];
    }
}
