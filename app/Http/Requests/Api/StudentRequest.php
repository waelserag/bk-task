<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Traits\ApiResponse;

class StudentRequest extends FormRequest
{
    use ApiResponse;

    protected function failedValidation(Validator $validator)
    {
       throw new HttpResponseException($this->error("Validation Error", 400, $validator->messages()->all()));
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'name'      => 'required|string|min:3|max:255',
            'school_id' => 'required|exists:schools,id',
        ];
    }

    /**
     * Rename Attribute
     */
    public function attributes()
    {
        return [
            'school_id' => 'school',
        ];
    }
}
