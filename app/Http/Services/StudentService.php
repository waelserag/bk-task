<?php

namespace App\Http\Services;

use App\Models\Student;
use Symfony\Component\HttpFoundation\Request;

class StudentService
{
    /**
     * Student Service for creation and editing
     *
     * @param  Symfony\Component\HttpFoundation\StudentRequest  $request
     * @param  \App\Models\Student  $student (nullable)
     * @return \App\Models\Student
     */
    public function fillFromRequest(Request $request, $student = null) :Student
    {
        // In case of editing
        if ($student) {
            $id = $student->id;
            //Check if This same school dont increase order
            $increase_order = $this->checkOrder($request->school_id, $student->school_id);
            $request['order'] = $this->getNextOrderForSchool((int) $request->school_id, $student->order, $increase_order);
            Student::find($id)->update($request->all());
            return $student;
        }
        // In case of creation
        $request['order'] = $this->getNextOrderForSchool((int) $request->school_id);
        $student = Student::create($request->all());
        return $student;
    }

    /**
     * Get the next order for the school
     *
     * @param  int  $school_id
     * @param  int  $student_id
     * @param  bool $increase
     * @return int
     */
    public function getNextOrderForSchool(int $school_id, int $current_order = 1, bool $increase = true) :int
    {
        if ($increase) {
            $student = Student::where('school_id', $school_id)->orderBy('order', 'desc')->first();
            $next_order = $student ? $student->order+1 : 1;
        } else {
            $next_order = $current_order;
        }
        return $next_order;
    }

    /** Check If this same order of school dont increase order
     *
     * @param  int  $school_id
     * @param  int  $request_school_id
     * @return bool
     */
    public function checkOrder(int $request_school_id, int $school_id) :bool
    {
        $increase_order = true;
        if ($request_school_id == $school_id) {
            $increase_order = false;
        }
        return $increase_order;
    }
}
