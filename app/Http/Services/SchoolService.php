<?php

namespace App\Http\Services;

use App\Models\School;
use Symfony\Component\HttpFoundation\Request;

class SchoolService
{
    /**
     * School Service for creation and editing
     *
     * @param  Symfony\Component\HttpFoundation\SchoolRequest  $request
     * @param  \App\Models\School  $school (nullable)
     * @return \App\Models\School
     */
    public function fillFromRequest(Request $request, $school = null) :School
    {
        // In case of editing
        if ($school) {
            $id = $school->id;
            School::find($id)->update($request->all());
            return $school;
        }
        // In case of creation
        $school = School::create($request->all());
        return $school;
    }

}
